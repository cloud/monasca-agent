FROM centos:8

LABEL maintainer="MetaCentrum Cloud Team <cloud@ics.muni.cz>" \
      version="0.0.2" \
      description="Monasca Agent with LibVirt support"

RUN dnf -y update && dnf -y install python3 libvirt-devel gcc python3-devel \
    && pip3 install --upgrade pip &&  pip3 install monasca-agent[libvirt] \
    && dnf -y remove gcc python3-devel

COPY --from=gosu/assets /opt/gosu /opt/gosu
RUN set -x \
    && /opt/gosu/gosu.install.sh \
    && rm -fr /opt/gosu

RUN groupadd -g 42400 kolla \
    && useradd  -u 42431 -G kolla -s /usr/sbin/nologin monasca

COPY scripts/start_monasca.sh scripts/set_configs.sh /usr/local/bin/

RUN chmod 755 /usr/local/bin/start_monasca.sh /usr/local/bin/set_configs.sh

ENTRYPOINT ["/usr/local/bin/start_monasca.sh"]
CMD ["monasca-collector","foreground"]
