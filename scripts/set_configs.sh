#!/bin/bash
set -o errexit

RO_CONF_DIR="/var/lib/monasca/config_files"
CONF_DIR="/etc/monasca/agent"
LOG_DIR="/var/log/kolla/monasca"

if [ ! -f "$RO_CONF_DIR/agent.yaml" ]; then
    echo "Monasca agent config not found!"
    exit 1
fi

mkdir -p $CONF_DIR
chmod 755 $CONF_DIR

cp $RO_CONF_DIR/agent.yaml $CONF_DIR/agent.yaml
chmod 600 $CONF_DIR/agent.yaml
chown monasca:monasca $CONF_DIR/agent.yaml

mkdir -p $CONF_DIR/conf.d
chmod 755 $CONF_DIR/conf.d
chown monasca:monasca $CONF_DIR/conf.d

cp $RO_CONF_DIR/plugins/* $CONF_DIR/conf.d
if [ "$(ls -A $CONF_DIR/conf.d)" ]; then
  chmod 600 $CONF_DIR/conf.d/*
  chown monasca:monasca $CONF_DIR/conf.d/*
fi

mkdir -p $LOG_DIR
chown monasca:kolla $LOG_DIR
chmod 755 $LOG_DIR

touch $LOG_DIR/agent-custom.log
chown monasca:kolla $LOG_DIR/agent-custom.log
chmod 644 $LOG_DIR/agent-custom.log
