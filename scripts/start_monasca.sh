#!/bin/bash
set -o errexit

if [ "$1" = 'monasca-collector' ]; then
    set_configs.sh
    exec gosu monasca "$@"
fi

exec "$@"
